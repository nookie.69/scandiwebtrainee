# Junior Developer Test Task
## This is requirement task for job position at Scandiweb maded by: Goce Jordanoski.

## Installation

1. Clone the repo `git clone https://gitlab.com/nookie.69/scandiwebtrainee.git`
2. Open `XAMPP`
3. Start `Apache` and `MySql` from `XAMPP`
4. Open `http://localhost/phpmyadmin/`
5. Create Database with name `scandiweb`
6. Import the file `scandiweb.sql` into database
7. Open file `index.php` in your IDE (`VSCode reccomended`)
8. Right click in the file and choose `PHP Server: Serve project`
9. All set
10. Enjoy the project
 
### External links to the project
`main page`
```sh
https://scandiwebtrainee.000webhostapp.com/
```
`add product page`
```sh
https://scandiwebtrainee.000webhostapp.com/add-product
```