<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Scandiweb Add Product</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="./Style/style.css">
</head>

<body>
    <form action="./Products/Product_add.php" method="POST" id="product_form">

        <div class="container">
            <div class="hero">
                <div class="row align-items-center mt-5">
                    <div class="col-sm-6">
                        <h2>Product Add</h2>
                    </div>
                    <div class="col-sm-6 text-right">
                        <input type="submit" class="btn btn-success" value="Save">
                        <button type="button" class="btn btn-info" onclick="location.href = './index.php'" id="delete-product-btn">Cancel</button>
                    </div>
                    
                </div>

                <hr>

                <div class="row align-items-center flex-nowrap my-2">
                    <div class="col-sm-3">
                        <label for="sku" class="mr-3">SKU</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="sku" id="sku">
                    </div>
                </div>

                <div class="row align-items-center flex-nowrap my-2">
                    <div class="col-sm-3">
                        <label for="name" class="mr-3">Name</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="name" id="name">
                    </div>
                </div>

                <div class="row align-items-center flex-nowrap my-2">
                    <div class="col-sm-3">
                        <label for="price" class="mr-3">Price</label>
                    </div>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="price" id="price">
                    </div>
                </div>

                <div class="row align-items-center flex-nowrap my-2">
                    <div class="col-sm-3">
                        <label for="productType">Type Switcher</label>
                    </div>
                    <div class="col-sm-9">
                        <select id="productType" name="productType" class="form-control">
                            <option value="" hidden selected>Type Switcher</option>
                            <option value="DVD">DVD</option>
                            <option value="Book">Book</option>
                            <option value="Furniture">Furniture</option>
                        </select>
                    </div>
                </div>

                <div id="DVD" class="d-none">
                    <div class="row align-items-center flex-nowrap my-2">
                        <div class="col-sm-3">
                            <label for="size">Size (MB)</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="size" id="size">
                        </div>
                    </div>
                    <p>* Please, provide size in MB *</p>

                </div>

                <div id="Book" class="d-none">
                    <div class="row align-items-center flex-nowrap my-2">
                        <div class="col-sm-3">
                            <label for="weight">Weight (KG)</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="weight" id="weight">
                        </div>
                    </div>
                    <p>* Please, provide weight in KG *</p>

                </div>

                <div id="Furniture" class="d-none">
                    <div class="row align-items-center flex-nowrap my-2">
                        <div class="col-sm-3 d-flex  jsutify-content-center flex-column">
                            <label for="height" class="my-2 d-block">Height (CM)</label>
                            <label for="width" class="my-2 d-block">Width (CM)</label>
                            <label for="length" class="my-2 d-block">Length (CM)</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="form-control my-2" name="height" id="height">
                            <input type="text" class="form-control my-2" name="width" id="width">
                            <input type="text" class="form-control my-2" name="length" id="length">
                        </div>
                    </div>
                    <p>* Please, provide dimensions in H x W x L *</p>
                </div>
            </div>

            <hr>
            
            <div class="footer text-center">
                <p>Scandiweb Test Assignment</p>
            </div>
        </div>
    </form>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script src="./Products/addProduct.js"></script>
</body>

</html>