<?php
include('./Product_handler.php');

interface TypeStore {
    public function table($request);
}

class ProductStore extends Product {

    public function insert($request) {
        $this->set($request);
        $this->store();
    }

}

class DVD extends ProductStore implements TypeStore {
    public function table($request) {
        $this->insert($request);

        $sql = "INSERT INTO `dvd`(`sku_key`, `size`) VALUES (:sku, :size)";
        $query = $this->connect()->prepare($sql);

        $query->bindParam(':sku', $this->getSku());
        $query->bindParam(':size', $this->getSize());
        $query->execute();
    }
}

class Book extends ProductStore implements TypeStore {
    public function table($request) {
        $this->insert($request);

        $sql = "INSERT INTO `book`(`sku_key`, `weight`) VALUES (:sku, :weight)";
        $query = $this->connect()->prepare($sql);

        $query->bindParam(':sku', $this->getSku());
        $query->bindParam(':weight', $this->getWeight());
        $query->execute();
    }
}

class Furniture extends ProductStore implements TypeStore {
    public function table($request) {
        $this->insert($request);

        $sql = "INSERT INTO `furniture`(`sku_key`, `height`, `width`, `length`) VALUES (:sku, :height, :width, :length)";
        $query = $this->connect()->prepare($sql);

        $query->bindParam(':sku', $this->getSku());
        $query->bindParam(':height', $this->getHeight());
        $query->bindParam(':width', $this->getWidth());
        $query->bindParam(':length', $this->getLength());
        $query->execute();
    }
}

function type(TypeStore $object) {
    echo json_encode($object->table($_POST));
}

    $request = new $_POST['productType']();
    type($request);

    header('Location: ../index.php');
?>