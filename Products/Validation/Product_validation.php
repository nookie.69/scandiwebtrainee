<?php
include('../Product_handler.php');

interface TypeValidation {
    public function rules($request);
}

class ProductValidate extends Product {

    public function request($request) {
        $this->set($request);
        return $this->validation();
    }

}

class DVD extends ProductValidate implements TypeValidation {
    public function rules($request) {
        $error = $this->request($request);

        if(empty($this->getSize())) {
            array_push($error, ['size' => 'Size is required']);
        } else if(!is_numeric($this->getSize())) {
            array_push($error, ['size' => 'Size must be numerical']);
        }

        $json = ['error' => $error];
        return $json;
    }
}

class Book extends ProductValidate implements TypeValidation {
    public function rules($request) {
        $error = $this->request($request);

        if(empty($this->getWeight())) {
            array_push($error, ['weight' => 'Weight is required']);
        } else if(!is_numeric($this->getWeight())) {
            array_push($error, ['weight' => 'Weight must be numerical']);
        } 

        $json = ['error' => $error];
        return $json;
    }
}

class Furniture extends ProductValidate implements TypeValidation {
    public function rules($request) {
        $error = $this->request($request);

        if(empty($this->getHeight())) {
            array_push($error, ['height' => 'Height is required']);
        } else if(!is_numeric($this->getHeight())) {
            array_push($error, ['height' => 'Height must be numerical']);
        }

        if(empty($this->getWidth())) {
            array_push($error, ['width' => 'Width is required']);
        } else if(!is_numeric($this->getWidth())) {
            array_push($error, ['width' => 'Width must be numerical']);
        }

        if(empty($this->getLength())) {
            array_push($error, ['length' => 'Length is required']);
        } else if(!is_numeric($this->getLength())) {
            array_push($error, ['length' => 'Length must be numerical']);
        }

        $json = ['error' => $error];
        return $json;
    }
}

function type(TypeValidation $object) {
    echo json_encode($object->rules($_POST));
}

    if(!empty($_POST['productType'])){
        $validate = new $_POST['productType']();
        type($validate);
    } else {
        $validate = new ProductValidate();
        echo json_encode(['error' => $validate->request($_POST)]);
    }
?>