<?php

abstract class Dbms {

    private $servername;
    private $dbname;
    private $username;
    private $password;

    protected function connect() {
        $this->servername = 'localhost';
        $this->dbname = 'scandiweb';
        $this->username = 'root';
        $this->password = '';

        // $conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        $conn = new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
        return $conn;
    }

    abstract protected function read();
    abstract protected function delete($arr);
    abstract protected function store();

}

?>