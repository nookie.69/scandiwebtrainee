<?php

include('Product_handler.php');

class ProductView extends Product {
    public function viewAllProducts() {
        $datas = $this->read();
        if(is_array($datas)) {
            foreach($datas as $data) {
                echo "
                <div class='product border'>
                    <input class='delete-checkbox' type='checkbox' name='delete-product[]' id='' value='{$data["sku"]}'>
                    <div class='text-center'>
                        <p class='m-0'>{$data["sku"]}</p>
                        <p class='m-0'>{$data["name"]}</p>
                        <p class='m-0'>{$data["price"]} $</p>
                        <p class='m-0'>{$data["attribute"]}</p>
                    </div>
                </div>";
            }
        }
    }
}

$product = new ProductView();
return $product->viewAllProducts();

?>