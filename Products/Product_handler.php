<?php

include 'Dbms.php';

class Product extends Dbms {

    private $sku;
    private $name;
    private $price;
    private $productType;
    private $size;
    private $weight;
    private $height;
    private $width;
    private $length;

    protected function set($request) {
        $this->setSku($request['sku']);
        $this->setName($request['name']);
        $this->setPrice($request['price']);
        $this->setProductType($request['productType']);
        $this->setSize($request['size']);
        $this->setWeight($request['weight']);
        $this->setHeight($request['height']);
        $this->setWidth($request['width']);
        $this->setLength($request['length']);
    }

    protected function read() {
        $sql = "    SELECT  p.sku as sku, p.name as name, p.price as price, 
                            COALESCE( concat('Weight: ',b.weight, ' KG'), 
                                      concat('Dimensions: ',f.width,'x',f.height,'x',f.length), 
                                      concat('Size: ', d.size, ' MB')) as attribute
                    FROM product p
                    LEFT JOIN book b ON sku = b.sku_key
                    LEFT JOIN furniture f ON sku = f.sku_key
                    LEFT JOIN dvd d ON sku = d.sku_key
                    order by sku asc";
        
        $result = $this->connect()->query($sql);
        $numRows = $result->rowCount();
        
        if($numRows > 0) {
            while($row = $result->fetch()) {
                $data[] = $row;
            }
            return $data;
        }

    }

    protected function delete($arr) {
        foreach($arr as $value) {
            $sql = "DELETE FROM `product` WHERE `sku` = '{$value}'";
            $this->connect()->query($sql);
        }
    }

    protected function store() {
        $sql = "INSERT INTO `product`(`sku`, `name`, `price`) VALUES (:sku, :name, :price)";
        $query = $this->connect()->prepare($sql);

        $query->bindParam(':sku', $this->getSku());
        $query->bindParam(':name', $this->getName());
        $query->bindParam(':price', $this->getPrice());
        $query->execute();
    }

    protected function validation() {
        $error = [];
        
        if(empty($this->getSku())) {            
            array_push($error, ['sku' => 'SKU is required']);
        } else if($this->checkUnique($this->getSku()) > 0){
            array_push($error, ['sku' => 'SKU must be unique']);
        } 

        if(empty($this->getName())) {
            array_push($error, ['name' => 'Name is required']);
        }

        if(empty($this->getPrice())) {
            array_push($error, ['price' => 'Price is required']);
        } else if(!is_numeric($this->getPrice())){
            array_push($error, ['price' => 'Price must be numerical']);
        }

        if(empty($this->getProductType())) {
            array_push($error, ['productType' => 'Product is required']);
        }

        return $error;
    }

    public function checkUnique() {
        $sql = " SELECT * FROM product WHERE `sku` = '" . $this->getSku() . "'";
        
        $result = $this->connect()->query($sql);
        $numRows = $result->rowCount();
        
        return $numRows;
    }

    public function getSku()
    {
        return $this->sku;
    }

    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    public function getWidth()
    {
        return $this->width;
    }


    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    public function getLength()
    {
        return $this->length;
    }

    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }


    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    public function getProductType()
    {
        return $this->productType;
    }

    public function setProductType($productType)
    {
        $this->productType = $productType;

        return $this;
    }
}

?>