$(function () {
    $options = ['DVD', 'Book', 'Furniture']

    $('#productType').on('click', function() {
        $options.forEach(element => {
            if(element == $(this).children("option:selected").val()) {
                $("#" + element).removeClass('d-none')
            } else {
                $("#" + element).addClass('d-none')
            }
        });
    })

    var frm = $('#product_form');

    frm.submit(function (e) {
        e.preventDefault();
        
        $('.error').remove()

        $.ajax({
            type: frm.attr('method'),
            url: './Products/Validation/Product_validation.php',
            data: frm.serialize(),
            success: function (data) {
                console.log('Submission was successful.');
                data = JSON.parse(data);
                let errors = data.error;
                if(errors.length > 0) {
                    errors.forEach(element => {
                        $('#' + Object.keys(element)).after('<p class="text-danger m-0 error">' + Object.values(element) + '</p>')
                    })
                } else {
                    document.getElementById("product_form").submit();
                }
                
                
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            },
        });
    } )

})