$(async function() {
    let sku = [];
    
    function chck() {
        $('input:checkbox').on('change', function() {
            let name = $(this)[0].defaultValue;
            if ($(this).is(':checked')) {
            sku.push(name);
            } else {
            sku.splice(sku.indexOf(name), 1);
            }
        });
    }

    await $.ajax({    
        type: "GET",
        url: "Products/Product_view.php",             
        dataType: "html",                  
        success: (data) => {                    
            $(".products").html(data); 
            chck();
        }
    });

    $('#delete-product-btn').on('click', () => {
        let jsonString = JSON.stringify(sku);
        $.ajax({
             type: "POST",
             url: "Products/Product_delete.php",
             data: {data : jsonString}, 
             cache: false,
     
             success: function(data) {
                $.ajax({    
                    type: "GET",
                    url: "Products/Product_view.php",             
                    dataType: "html",                  
                    success: function(data){                    
                        $(".products").html(data); 
                        chck();
                    }
                });
             },
             error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 500) {
                    alert('Internal error: ' + jqXHR.responseText);
                } else {
                    alert('Unexpected error.');
                }
            }
        });
    })

})